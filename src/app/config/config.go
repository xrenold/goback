package config

import (
	"os"
	// "strconv"
	//    "strings"
)

const SERVER = "mongodb://localhost:27017/dummystore"
const DBNAME = "dummyStore"
const TOKENSECRET = "secret"
const TOKENTIMEOUT = 60 * 24 //minutes

func Get(key string) string {
	if value, exists := os.LookupEnv(key); exists {
		return value
	} else {
		return ""
	}
}
