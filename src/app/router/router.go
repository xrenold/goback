package router

import (
	// "log"
	"net/http"

	"github.com/gorilla/mux"

	//middlewares
	"app/middlewares"
	//controllers
	"app/adapter"
	"app/subs/imagery"
	"app/subs/imgops"
	"app/subs/store"
	"app/subs/todo"
	"app/subs/user"
)

// define list of routes

type Routes []Route

var initHeaders = map[string]string{
	// "Content-Type" : "application/json; charset=UTF-8",
	// "Access-Control-Allow-Origin" : "*",
	// "Access-Control-Allow-Methods": "POST, GET, OPTIONS, PUT, DELETE",
	// "Access-Control-Allow-Headers": "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, X-Requested-With, Access-Control-Request-Headers",
}

var storeController store.Controller
var userController user.Controller

var todoController todo.Controller
var imageryController imagery.Controller
var imgopsController imgops.Controller

type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

var routes = Routes{

	Route{
		"Index",
		"POST",
		"/",
		storeController.Index,
	},
	Route{
		"GetProducts",
		"GET",
		"/get-products",
		middlewares.AuthenticationMiddleware(storeController.GetProducts),
	},
	Route{
		"Json common parser test",
		"POST",
		"/place-data",
		middlewares.AuthenticationMiddleware(storeController.PlaceData),
	},

	Route{
		"Generate Token",
		"POST",
		"/login",
		userController.GetToken,
	},
	Route{
		"Register User",
		"POST",
		"/signup",
		userController.Register,
	},

	Route{
		"Add todo",
		"POST",
		"/add-todo",
		middlewares.AuthenticationMiddleware(todoController.AddTodo),
	},
	Route{
		"Get todos",
		"GET",
		"/get-todos",
		middlewares.AuthenticationMiddleware(todoController.GetTodos),
	},
	Route{
		"Get todo",
		"GET",
		"/get-todo",
		middlewares.AuthenticationMiddleware(todoController.GetTodo),
	},
	Route{
		"Delete todo",
		"DELETE",
		"/todo",
		middlewares.AuthenticationMiddleware(todoController.DeleteTodo),
	},

	Route{
		"Get image",
		"POST",
		"/imagery",
		imageryController.GetImage,
	},

	Route{
		"Get user image",
		"POST",
		"/get-image",
		middlewares.AuthenticationMiddleware(imgopsController.GetImage),
	},
	Route{
		"bw image",
		"POST",
		"/bw",
		middlewares.AuthenticationMiddleware(imgopsController.GetBW),
	},
	Route{
		"reset image",
		"POST",
		"/reset",
		middlewares.AuthenticationMiddleware(imgopsController.Reset),
	},
	Route{
		"get signed url",
		"POST",
		"/download",
		middlewares.AuthenticationMiddleware(imgopsController.GetDownloadUrl),
	},
}

func NewRouter() *mux.Router {
	router := mux.NewRouter().StrictSlash(true)
	for _, route := range routes {
		var handler http.Handler
		// log.Println(route.Name)
		handler = adapter.Adapt(route.HandlerFunc, middlewares.ResponseInit(initHeaders), middlewares.Notify(route.Name))
		router.PathPrefix("/api/").Methods(route.Method).Path(route.Pattern).Name(route.Name).Handler(handler)
	}
	// router.Path("/").Handler(http.FileServer(http.Dir("_static/")))
	return router
}
