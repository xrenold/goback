package adapter

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"log"
	"net/http"
)

type Adapter func(http.Handler) http.Handler

func Adapt(h http.Handler, adapters ...Adapter) http.Handler {
	for _, adapter := range adapters {
		h = adapter(h)
	}
	return h
}

func GetBody(body io.Reader) ([]map[string]interface{}, error) {
	data, err := ioutil.ReadAll(body)
	log.Println("data", string(data))
	var result []map[string]interface{}
	if err != nil {
		log.Println("error", err)
	}
	err = json.Unmarshal(data, &result)
	if err != nil {
		log.Println("marshal error", err)
	}
	log.Println("data getbody", result)
	return result, err
}

func GetStructuredBody(body io.Reader, model interface{}) {
	data, err := ioutil.ReadAll(body)
	if err != nil {
		log.Fatal("error", err)
	}
	log.Println("data", string(data))
	log.Println("model", model)

	err = json.Unmarshal(data, &model)
	if err != nil {
		log.Fatal("error", err)
	}
	log.Println("result", model)
	return
}
