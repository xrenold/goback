package todo

import (
	"gopkg.in/mgo.v2/bson"
)

type Todo struct {
	Id bson.ObjectId `bson:"_id,omitempty"`
	Title string `json:"title"`
	Desc string `json:"description"`
	User string `json:"user_id"`
}

type Todos []Todo