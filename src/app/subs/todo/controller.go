package todo

import (
	"log"
	"encoding/json"
	"net/http"
	"github.com/gorilla/context"
    // "github.com/dgrijalva/jwt-go"
)

type Controller struct {}

var repository Repository 

func (c *Controller) AddTodo(w http.ResponseWriter, req *http.Request) {
	user := context.Get(req, "user")
	var todo Todo 
	err := json.NewDecoder(req.Body).Decode(&todo)
	defer req.Body.Close()
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(&err)
		return
	}
	todo.User = user.(string)
	err = repository.AddTodo(todo)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(err)
		return 
	}
	w.WriteHeader(http.StatusOK)
	responseData := struct {
		Success bool
	}{true}
	json.NewEncoder(w).Encode(responseData)
	return
}

func (c *Controller) GetTodos(w http.ResponseWriter, req *http.Request) {
	user := context.Get(req, "user")
	log.Println("get todo", user)
	todos, err := repository.GetTodos(user.(string))
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(err)
		return	
	}
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(todos)
	return
}

func (c *Controller) GetTodo(w http.ResponseWriter, req *http.Request) {
	user := context.Get(req, "user")
	id:= req.URL.Query()["id"][0]
	log.Println("log", id,)
	if id == "" {
		w.WriteHeader(http.StatusBadRequest)
		return 
	}
	todo, err := repository.GetTodo(user.(string), id)
	if err != nil{
		log.Println("error", err)
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(struct{
			Message string
		}{err.Error()})
		return
	}
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(todo)
}

func (c *Controller) DeleteTodo(w http.ResponseWriter, req *http.Request) {
	user := context.Get(req, "user")
	log.Println("get todo", user)
	id := req.URL.Query()["id"][0]
	if id == "" {
		w.WriteHeader(http.StatusBadRequest)
		return	
	}
	err := repository.DeleteTodo(user.(string), id)
	if err != nil {
		log.Println("error", err)
		if err.Error() == "Not Found" {
			w.WriteHeader(http.StatusNotFound)	
		} else {
			w.WriteHeader(http.StatusInternalServerError)
		}
		json.NewEncoder(w).Encode(struct{
			Message string
		}{err.Error()})
		return
	}
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(struct{
		Success bool
	}{true})
	return
}