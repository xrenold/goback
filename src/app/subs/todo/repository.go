package todo

import (
	"log"
	// "fmt"
	"app/config"
	"errors"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Repository struct{}

const COLLECTION = "todo"

func (r *Repository) AddTodo(todo Todo) error {
	session, err := mgo.Dial(config.SERVER)
	if err != nil {
		return err
	}
	defer session.Close()
	collection := session.DB(config.DBNAME).C(COLLECTION)
	index := mgo.Index{
		Key: []string{"title"},
	}
	err = collection.EnsureIndex(index)
	if err != nil {
		return err
	}
	err = collection.Insert(&todo)
	return err
}

func (r *Repository) GetTodos(userId string) (Todos, error) {
	log.Println("get todo", userId)
	session, err := mgo.Dial(config.SERVER)
	if err != nil {
		return Todos{}, err
	}
	defer session.Close()
	collection := session.DB(config.DBNAME).C(COLLECTION)
	var todos Todos
	err = collection.Find(bson.M{"user": userId}).All(&todos)
	if err != nil {
		return Todos{}, err
	}
	return todos, nil
}

func (r *Repository) GetTodo(userId string, id string) (Todo, error) {
	log.Println("get todo", userId, id)
	if !bson.IsObjectIdHex(id) {
		return Todo{}, errors.New("Corrupted id")
	}
	session, err := mgo.Dial(config.SERVER)
	if err != nil {
		log.Println("err", err)
		return Todo{}, err
	}
	defer session.Close()
	collection := session.DB(config.DBNAME).C(COLLECTION)
	var todo Todo
	err = collection.Find(bson.M{"_id": bson.ObjectIdHex(id), "user": userId}).One(&todo)
	if err != nil {
		log.Println("err2", err)
		return Todo{}, err
	}
	return todo, nil
}

func (r *Repository) DeleteTodo(userId string, id string) error {
	if !bson.IsObjectIdHex(id) {
		return errors.New("Corrupted id")
	}
	session, err := mgo.Dial(config.SERVER)
	if err != nil {
		log.Println("err", err)
		return err
	}
	defer session.Close()
	collection := session.DB(config.DBNAME).C(COLLECTION)
	// var todo Todo
	log.Println("dleelte", id, userId)
	err = collection.Remove(bson.M{"_id": bson.ObjectIdHex(id), "user": userId})
	if err != nil {
		return err
	}
	return nil
}
