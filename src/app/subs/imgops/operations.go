package imgops

import (
	"app/config"
	"app/helpers"
	"fmt"
	"image"
	"image/jpeg"
	"os"
	"path/filepath"

	"github.com/anthonynsimon/bild/blur"
	"github.com/anthonynsimon/bild/effect"
)

func WriteImage(wImg image.Image, format, name string) {
	absPath, err := filepath.Abs("_static/images/")
	helpers.CheckError(err)
	var imagePath string
	if format == "" {
		imagePath = fmt.Sprintf("%s/%s", absPath, name)
	} else {
		imagePath = fmt.Sprintf("%s/%s.%s", absPath, name, format)
	}
	fg, err := os.Create(imagePath)
	defer fg.Close()
	helpers.CheckError(err)
	err = jpeg.Encode(fg, wImg, nil)
	helpers.CheckError(err)
}

func ReadImage(name string) *os.File {
	absPath, err := filepath.Abs("_static/images/")
	helpers.CheckError(err)
	imagePath := fmt.Sprintf("%s/%s", absPath, name)
	file, err := os.Open(imagePath)
	helpers.CheckError(err)
	return file
}

func GetImageUrl(name, format string) string {
	var path string
	if format == "" {
		path = fmt.Sprintf("%s/%s", config.Get("STATICIMAGE"), name)
	} else {
		path = fmt.Sprintf("%s/%s.%s", config.Get("STATICIMAGE"), name, format)
	}
	return path
}

func BW(img image.Image) image.Image {
	wImg := effect.Grayscale(img)
	return wImg
}

func Sepia(img image.Image) image.Image {
	return effect.Sepia(img)
}

func Blur(img image.Image) image.Image {
	// size := img.Bounds().Size()
	return blur.Gaussian(img, 8.0)
}

func Operate(img image.Image, operations []string) image.Image {
	var wImg image.Image
	wImg = img
	for _, value := range operations {
		switch value {
		case "bw":
			wImg = BW(wImg)
		case "sepia":
			wImg = Sepia(wImg)
		case "blur":
			wImg = Blur(wImg)
		}
	}
	return wImg
}
