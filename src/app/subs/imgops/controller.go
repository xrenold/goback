package imgops

import (
	"app/helpers"
	"app/helpers/s3"
	"bytes"
	"encoding/json"
	"fmt"
	"image"
	"image/jpeg"
	"image/png"
	"net/http"
	"strings"
)

type Controller struct{}

var repository Repository

func init() {
	image.RegisterFormat("jpeg", "jpeg", jpeg.Decode, jpeg.DecodeConfig)
	image.RegisterFormat("png", "png", png.Decode, png.DecodeConfig)
	// image.RegisterFormat("gif", "gif", gif.Decode, gif.DecodeConfig)
}

func (c *Controller) GetImage(w http.ResponseWriter, req *http.Request) {
	// user := context.Get(req, "user")
	file, header, err := req.FormFile("image")
	defer file.Close()
	response := s3.UploadFile(file, header.Filename)
	index := strings.LastIndex(header.Filename, ".")
	_ = s3.UploadFile(file, header.Filename[0:index]+"_original"+header.Filename[index:len(header.Filename)])
	helpers.CheckError(err)
	helpers.WriteResponse(w, http.StatusOK, response.Location)
}

func (c *Controller) GetBW(w http.ResponseWriter, req *http.Request) {
	var uImage UserImage
	err := json.NewDecoder(req.Body).Decode(&uImage)
	fmt.Println("image", uImage)
	helpers.CheckError(err)
	defer req.Body.Close()
	file := s3.GetFile(uImage.Name)
	img, _, err := image.Decode(file.Body)
	helpers.CheckError(err)
	wImg := Operate(img, uImage.Operations)
	var buff bytes.Buffer
	err = jpeg.Encode(&buff, wImg, nil)
	helpers.CheckError(err)
	response := s3.UploadFile(&buff, uImage.Name)

	link := response.Location //GetImageUrl(uImage.Name, "")
	defer helpers.WriteResponse(w, http.StatusOK, link)
}

func (c *Controller) Reset(w http.ResponseWriter, req *http.Request) {
	var uImage UserImage
	err := json.NewDecoder(req.Body).Decode(&uImage)
	helpers.CheckError(err)
	defer req.Body.Close()
	index := strings.LastIndex(uImage.Name, ".")
	file := s3.GetFile(uImage.Name[0:index] + "_original" + uImage.Name[index:len(uImage.Name)]) //ReadImage(uImage.Name[0:index] + "_original" + uImage.Name[index:len(uImage.Name)])
	response := s3.UploadFile(file.Body, uImage.Name)
	link := response.Location
	helpers.WriteResponse(w, http.StatusOK, link)
}

func (c *Controller) GetDownloadUrl(w http.ResponseWriter, req *http.Request) {
	var uImage UserImage
	err := json.NewDecoder(req.Body).Decode(&uImage)
	helpers.CheckError(err)
	defer req.Body.Close()

	urlStr := s3.GetSignedUrl(uImage.Name)
	helpers.WriteResponse(w, http.StatusOK, urlStr)
}
