package imgops

import (
	"app/config"
	"app/helpers"

	"gopkg.in/mgo.v2"
)

type Repository struct{}

const COLLECTION = "images"

func (r Repository) GetImage(imageName string, user string) {
	session, err := mgo.Dial(config.SERVER)
	helpers.CheckError(err)
	defer session.Close()
	collection := session.DB(config.DBNAME).C(COLLECTION)
	index := mgo.Index{
		Key: []string{"id", "user"},
	}
	err = collection.EnsureIndex(index)
	helpers.CheckError(err)
	var userImage = UserImage{Name: imageName, User: user}
	err = collection.Insert(&userImage)
	helpers.CheckError(err)
	return
}
