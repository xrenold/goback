package imgops

import "gopkg.in/mgo.v2/bson"

type UserImage struct {
	id         bson.ObjectId `bson:"_id,omitempty"`
	Name       string        `json:"name"`
	User       string        `json:"user"`
	Operations []string      `json:"ops"`
}
