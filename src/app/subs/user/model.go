package user

import (
	"gopkg.in/mgo.v2/bson"
)

type User struct {
	Id bson.ObjectId `bson:"_id,omitempty"`
    Username string `json:"username"`
    Password string `json:"password"`
}

type UserDetails struct {
	Id bson.ObjectId `bson:"_id,omitempty"`
    Username string `json:"username"`
}

type JwtToken struct {
    Token string `json:"token"`
}