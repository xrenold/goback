package user

import (
	"log"
	// "fmt"
	"app/config"
	"app/helpers"
	"errors"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Repository struct{}

const COLLECTION = "user"

func (r Repository) Register(user User) error {
	session, err := mgo.Dial(config.SERVER)
	if err == nil {
		defer session.Close()
		hashedPassword, hashError := helpers.HashAndSalt([]byte(user.Password))
		if hashError != nil {
			log.Fatal(err)
		}
		user.Password = hashedPassword
		// log.Println("hashedPassword", user)
		collection := session.DB(config.DBNAME).C(COLLECTION)
		index := mgo.Index{
			Key:    []string{"username"},
			Unique: true,
		}
		err = collection.EnsureIndex(index)
		if err != nil {
			return err
		}
		err = collection.Insert(&user)
		if err != nil {
			return err
		}
	}
	return err
}

func (r Repository) UserExist(user User) (UserDetails, error) {
	session, err := mgo.Dial(config.SERVER)
	if err != nil {
		return UserDetails{}, err
	}
	defer session.Close()
	// var userDetails UserDetails
	var dbUser User
	collection := session.DB(config.DBNAME).C(COLLECTION)
	err = collection.Find(bson.M{"username": user.Username}).One(&dbUser)
	if err != nil {
		return UserDetails{}, err
	}
	if helpers.ComparePasswords(dbUser.Password, []byte(user.Password)) {
		return UserDetails{dbUser.Id, dbUser.Username}, nil
	} else {
		return UserDetails{}, errors.New("Unauthorized")
	}
}
