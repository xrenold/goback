package user

import (
	"encoding/json"
	// "io"
	"fmt"
	// "io/ioutil"
	// "log"
	"net/http"
	// "strings"
	// "strconv"

	// "github.com/gorilla/mux"
	// "github.com/gorilla/context"
	"app/config"
	"time"

	"github.com/dgrijalva/jwt-go"
)

type Controller struct{}

var repository Repository

func (c *Controller) GetToken(w http.ResponseWriter, req *http.Request) {
	var user User
	_ = json.NewDecoder(req.Body).Decode(&user) //Simple way to decode json data
	defer req.Body.Close()
	var userDetails UserDetails
	userDetails, err := repository.UserExist(user)
	// fmt.Println("user", userDetails)
	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		json.NewEncoder(w).Encode(err) //Simple way to encode json data
		return
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"username": userDetails.Username,
		// "password": userDetails.Password,
		"_id": userDetails.Id,
		"exp": time.Now().Add(time.Minute * config.TOKENTIMEOUT).Unix(),
	})

	// log.Println("Id: " + userDetails.Id);
	// log.Println("Username: " + userDetails.Username);
	// log.Println("Password: " + userDetails.Password);

	tokenString, error := token.SignedString([]byte(config.TOKENSECRET))
	if error != nil {
		fmt.Println(error)
	}
	responseData := struct {
		Token string
		User  UserDetails
	}{Token: tokenString, User: userDetails}
	json.NewEncoder(w).Encode(responseData)
	return
}

func (c *Controller) Register(w http.ResponseWriter, req *http.Request) {
	var user User
	_ = json.NewDecoder(req.Body).Decode(&user)
	defer req.Body.Close()
	err := repository.Register(user)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(err)
		return
	}
	responseData := struct {
		Success bool
	}{true}
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(responseData)
	return
}
