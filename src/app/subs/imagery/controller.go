package imagery

import (
	// "log"
	"net/http"
	// "io/ioutil"
	// "encoding/json"
	"bytes"
	"encoding/base64"

	// "github.com/gorilla/context"
	"fmt"
	"image"
	"image/color"
	"image/draw"
	"image/jpeg"
	"image/png"
)

type Controller struct{}

func (c Controller) GetImage(w http.ResponseWriter, req *http.Request) {
	// fmt.Println("get image", req.Body)
	// req.ParseForm()
	file, header, err := req.FormFile("file")
	if err != nil {
		fmt.Println("error", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	defer file.Close()
	b := make([]byte, header.Size)
	n, err := file.Read(b)
	if err != nil {
		fmt.Println("error", err)
		return
	}
	fmt.Println("size", n, header.Size, header.Header["Content-Type"][0])
	var img image.Image
	var s error
	if header.Header["Content-Type"][0] == "image/png" {
		img, s = png.Decode(bytes.NewReader(b))
	} else {
		img, s = jpeg.Decode(bytes.NewReader(b))
	}
	// img, ok := img.(draw.Image)
	var newImg draw.Image
	// if !ok {
	// 	fmt.Println("not okay")
	// 	return
	// } else {
	bound := img.Bounds()
	fmt.Println("colormodel", img.ColorModel(), bound.Min.Y, bound.Max.Y)
	newImg = image.NewRGBA(image.Rect(bound.Min.X, bound.Min.Y, bound.Max.X, bound.Max.Y))
	for y := bound.Min.Y; y < bound.Max.Y; y++ {
		for x := bound.Min.X; x < bound.Max.X; x++ {
			// fmt.Println(img.At(x, y))
			R, G, B, A := img.At(x, y).RGBA()
			// if B > R && B > G {
			// 	B = 0
			// }
			if x < bound.Min.X+10 || x > bound.Max.X-10 || y < bound.Min.Y+10 || y > bound.Max.Y-10 {
				R = 0
				G = 255
				B = 0
			}
			newImg.Set(x, y, color.RGBA{uint8(R), uint8(G), uint8(B), uint8(A)})
		}
	}
	// }

	fmt.Println("image", s)
	var buff bytes.Buffer
	png.Encode(&buff, newImg)
	encodedString := base64.StdEncoding.EncodeToString(buff.Bytes())
	// fmt.Println("endoce", encodedString)
	htmlImage := "<html><body><img src=\"data:image/png;base64," + encodedString + "\" /></body></html>"
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(htmlImage))

	return
}
