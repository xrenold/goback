package store

import (
	"app/adapter"
	"bytes"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/gorilla/context"
)

type Controller struct{}

var repository Repository

var recaptcha Recaptcha

var response = User{"hello"}

func (c Controller) Index(w http.ResponseWriter, r *http.Request) {

	// w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	//    w.Header().Set("Access-Control-Allow-Origin", "*")
	data, err := ioutil.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	err = json.Unmarshal(data, &recaptcha)

	if err != nil {
		log.Println("error Unmarshal", err)
	}
	// log.Println("recaptcha", recaptcha.Token)
	// verify, _, err := http.Post("https://www.google.com/recaptcha/api/siteverify")
	requestBody, _ := json.Marshal(map[string]string{
		"response": recaptcha.Token,
		"secret":   "6LeejaoUAAAAAIkMJKOFMumP06AwtEdjrzdE5fKR",
	})
	// log.Println("request body", string(requestBody))
	responseBody, er := http.Post("https://www.google.com/recaptcha/api/siteverify?secret=6LeejaoUAAAAAIkMJKOFMumP06AwtEdjrzdE5fKR&&response="+recaptcha.Token, "application/json", bytes.NewBuffer(requestBody))
	if er != nil {
		log.Println("POST error", er)
	}
	defer responseBody.Body.Close()
	body, _ := ioutil.ReadAll(responseBody.Body)
	log.Println("\n body", string(body))

	w.WriteHeader(http.StatusOK)
	resdata, _ := json.Marshal(response)
	w.Write(resdata)
	// w.Write(json.Marshal({"status": 'OK'}))
	return
}

func (c Controller) GetProducts(w http.ResponseWriter, r *http.Request) {
	// w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	//    w.Header().Set("Access-Control-Allow-Origin", "*")

	log.Println("response has user", context.Get(r, "decoded"))
	products := repository.GetProducts()

	resdata, _ := json.Marshal(products)
	w.Write(resdata)
	return
}

func (c Controller) PlaceData(w http.ResponseWriter, r *http.Request) {
	log.Println("PlaceData")
	var userData = UserData{}
	adapter.GetStructuredBody(r.Body, userData)
	log.Println("controller model", userData)
	return
	// data, err := adapter.GetBody(r.Body)
	// if err != nil {
	// 	log.Fatal("error", err)
	// }
	// log.Println(data, "\nname", data)
}
