// Product represents an e-comm item
package store

type User struct {
	Name   string
}

type UserData struct {
	Name string 
	Age string
	// City struct {
	// 	name string
	// }
}

type Product struct {
	ID     int 		 	 `bson:"_id"`
	Title  string        `json:"title"`
	Image  string        `json:"image"`
	Price   uint64       `json:"price"`
	Rating  uint8        `json:"rating"`
}

// Products is an array of Product objects
type Products []Product

type Recaptcha struct {
	Token string `json: "token"`
}