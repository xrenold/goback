package store

import (
	"log"
	"fmt"
	// "net/http"
	// "io/ioutil"
	// "encoding/json"
	"gopkg.in/mgo.v2"
)

type Repository struct {}

const SERVER = "mongodb://localhost:27017/dummystore"
const DBNAME = "dummyStore"
const COLLECTION = "store"

// var productId = 10

func (r Repository) GetProducts() Products {
	session, err := mgo.Dial(SERVER)
	if(err != nil) {
		fmt.Println("error", err)
	}
	// log.Println("server", SERVER, session)

	defer session.Close()

	c := session.DB(DBNAME).C(COLLECTION)

	log.Println("collection", c)

	results := Products {}

	if err := c.Find(nil).All(&results); err != nil {
		fmt.Println("Failed to write results")
	}

	log.Println("results", results)

	return results 
}