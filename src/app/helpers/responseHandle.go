package helpers

import (
	"encoding/json"
	"net/http"
)

type Message struct {
	Message string `json:"message"`
}

func WriteResponse(w http.ResponseWriter, status int, message string) {
	w.WriteHeader(status)
	if message != "" {
		json.NewEncoder(w).Encode(Message{message})
	}
}
