package helpers

import "net/http"

func CheckError(err error) {
	if err != nil {
		panic(err)
	}
}

func CheckErrorWithResponse(err error, w http.ResponseWriter, status int, message string) {
	if err != nil {
		defer panic(err)
		WriteResponse(w, status, message)
	}
}
