package s3

import (
	"app/config"
	"app/helpers"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"time"

	"github.com/aws/aws-sdk-go/aws/client"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
)

//InitS3 is ....
func InitS3() (*s3.S3, client.ConfigProvider) {
	sess, err := session.NewSession(&aws.Config{
		Region:      aws.String(config.Get("AWS_REGION")),
		Credentials: credentials.NewStaticCredentials(config.Get("AWS_KEY"), config.Get("AWS_SECRET"), ""),
	},
	)
	helpers.CheckError(err)
	svc := s3.New(sess)
	return svc, sess
}

func ListInBucket() {
	svc, _ := InitS3()
	bucket := config.Get("AWS_BUCKET")
	resp, err := svc.ListObjectsV2(&s3.ListObjectsV2Input{Bucket: aws.String(bucket)})
	helpers.CheckError(err)

	for _, item := range resp.Contents {
		fmt.Println("Name:         ", *item.Key)
		fmt.Println("Last modified:", *item.LastModified)
		fmt.Println("Size:         ", *item.Size)
		fmt.Println("Storage class:", *item.StorageClass)
		fmt.Println("")
	}
}

func UploadFile(file io.Reader, filename string) *s3manager.UploadOutput {
	_, sess := InitS3()
	bucket := config.Get("AWS_BUCKET")
	uploader := s3manager.NewUploader(sess)
	response, err := uploader.Upload(&s3manager.UploadInput{
		Bucket: aws.String(bucket),
		Key:    aws.String("/uploads/" + filename),
		Body:   file,
		ACL:    aws.String("public-read"),
	})
	helpers.CheckError(err)
	return response
}

func DownloadFile(name string) {
	_, sess := InitS3()
	bucket := config.Get("AWS_BUCKET")
	filename := "/uploads/" + name
	absPath, _ := filepath.Abs("_static/images/")
	downloader := s3manager.NewDownloader(sess)
	file, err := os.Create(absPath + "/" + name)
	helpers.CheckError(err)
	_, err = downloader.Download(file, &s3.GetObjectInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(filename),
	})
	helpers.CheckError(err)
}

func GetFile(name string) s3.GetObjectOutput {
	svc, _ := InitS3()
	bucket := config.Get("AWS_BUCKET")
	filename := "/uploads/" + name
	results, err := svc.GetObject(&s3.GetObjectInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(filename),
	})
	helpers.CheckError(err)
	return *results
}

func GetSignedUrl(name string) string {
	svc, _ := InitS3()
	bucket := config.Get("AWS_BUCKET")
	filename := "/uploads/" + name

	req, _ := svc.GetObjectRequest(&s3.GetObjectInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(filename),
	})
	urlStr, err := req.Presign(15 * time.Minute)
	fmt.Println("before url")
	if err != nil {
		helpers.CheckError(err)
	}

	log.Println("url", urlStr)
	return urlStr
}
