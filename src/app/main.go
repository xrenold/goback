package main

import (
	"app/config"
	"app/router"
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/gorilla/handlers"
	"github.com/joho/godotenv"
)

func init() {
	// loads values from .env into the system
	if err := godotenv.Load(); err != nil {
		log.Print("No .env file found")
	}
	fmt.Println("DBSERVER", config.Get("SERVER"))
}

func main() {
	// get the port env variable

	port := os.Getenv("PORT")
	if port == "" {
		log.Fatal("$PORT must be set")
	}

	router := router.NewRouter() // creates new routes
	router.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir("_static"))))
	allowedOrigins := handlers.AllowedOrigins([]string{"*"})
	allowedHeaders := handlers.AllowedHeaders([]string{"content-type", "authorization"})
	allowedMethods := handlers.AllowedMethods([]string{"GET", "POST", "PUT", "DELETE", "OPTIONS"})
	// http.Handle("/jo/", handlers.CORS(allowedOrigins, allowedHeaders, allowedMethods)(http.StripPrefix("/jo/", http.FileServer(http.Dir("static")))))
	// http.Handle("/", handlers.CORS(allowedOrigins, allowedHeaders, allowedMethods)(router))
	//Launch server with CORS validations
	log.Fatal(http.ListenAndServe(":"+port, handlers.CORS(allowedOrigins, allowedHeaders, allowedMethods)(router)))
}
