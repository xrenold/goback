package middlewares

import (
	"app/adapter"
	"log"
	"net/http"
)

func ResponseInit(headers map[string]string) adapter.Adapter {

	return func(h http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			log.Println("falkdfjalkfjalf")
			for key, value := range headers {
				w.Header().Set(key, value)
			}
			// w.Header().Set("Content-Type", headers["Content-Type"])
			// w.Header().Set("Access-Control-Allow-Origin", headers["Access-Control-Allow-Origin"])
			log.Println("init headers")
			h.ServeHTTP(w, r)
		},
		)
	}
}

func Notify(name string) adapter.Adapter {
	return func(h http.Handler) http.Handler {
		return http.HandlerFunc(
			func(w http.ResponseWriter, r *http.Request) {
				log.Println(name, "\nbefore\n")
				defer log.Println("\n\nafter\n")
				h.ServeHTTP(w, r)
			},
		)
	}
}
