package middlewares

import (
	"encoding/json"
	// "io"
	// "io/ioutil"
	"fmt"
	"log"
	"net/http"
	"strings"

	// "strconv"

	// "github.com/gorilla/mux"
	"github.com/dgrijalva/jwt-go"
	"github.com/gorilla/context"
)

type Exception struct {
	Message string `json:"message"`
}

// func AuthMiddleware () adapter.Adapter {
// 	return func(h http.Handler) http.Handler {
// 		return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
// 			authorizationHeader := req.Header.Get("authorization")
// 			if authorizationHeader != "" {
// 				bearerToken := strings.Split(authorizationHeader, " ")
// 				if len(bearerToken) == 2 {
// 					token, error := jwt.Parse(bearerToken[1], func(token *jwt.Token) (interface {}, error) {
// 						if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
// 							return nil, fmt.Errorf("There was an error")
// 						}
// 						return []byte("secret"), nil
// 					})
// 					if error != nil {
// 						json.NewEncoder(w).Encode(Exception{Message: error.Error()})
//                     	return
// 					}
// 					if token.Valid {
// 						log.Println("Token is valid")
// 						context.Set(req, "decoded", token.Claims)
// 						h.ServeHTTP(w, req)
// 					} else {
// 						json.NewEncoder(w).Encode(Exception{Message: "Invalid authorization token"})
// 					}
// 				}
// 			} else {
// 				json.NewEncoder(w).Encode(Exception{Message: "An authorization header is required"})
// 			}
// 		},)
// 	}
// }

func AuthenticationMiddleware(next http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		authorizationHeader := req.Header.Get("authorization")
		// fmt.Println("auth middleware", req.Header)
		if authorizationHeader != "" {
			bearerToken := strings.Split(authorizationHeader, " ")
			// fmt.Println("bearerToken", bearerToken, "\nauth header", authorizationHeader)
			// if len(bearerToken) == 2 {
			token, error := jwt.Parse(bearerToken[0], func(token *jwt.Token) (interface{}, error) { //bearerToken[1]
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					w.WriteHeader(http.StatusUnauthorized)
					return nil, fmt.Errorf("There was an error")
				}
				return []byte("secret"), nil
			})
			if error != nil {
				w.WriteHeader(http.StatusUnauthorized)
				json.NewEncoder(w).Encode(Exception{Message: error.Error()})

				return
			}
			if token.Valid {
				log.Println("TOKEN WAS VALID", token.Claims)
				context.Set(req, "decoded", token.Claims)
				context.Set(req, "user", jwt.MapClaims(token.Claims.(jwt.MapClaims))["_id"])
				next(w, req)
			} else {
				w.WriteHeader(http.StatusUnauthorized)
				json.NewEncoder(w).Encode(Exception{Message: "Invalid authorization token"})
			}
			// }
		} else {
			w.WriteHeader(http.StatusUnauthorized)
			json.NewEncoder(w).Encode(Exception{Message: "An authorization header is required"})
		}
	})
}
