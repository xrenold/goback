# REST API using GO

This is an attempt at developing a code structure for developing REST API using Go. This project also attempts to do basic image processing on source images, uploaded to s3.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

#### Go
If you’re a fan of details, follow the official [docs](https://golang.org/doc/install?source=post_page---------------------------) of installation. The process is quite easy.

I also came across this bash script to automate the Golang development setup:

1. Download the script using wget or your fav download manager
```bash
wget https://raw.githubusercontent.com/canha/golang-tools-install-script/master/goinstall.sh
```
2. Run the script
```bash
bash goinstall.sh
```
3. Set GOPATH environment variable if needed.

4. Test installation
```bash
go version
``` 

#### Dep
Dep is a package manager for Go projects.
To install, run: 
```bash
curl https://raw.githubusercontent.com/golang/dep/master/install.sh | sh
```

#### Mongo DB
See the [official docs](https://docs.mongodb.com/manual/installation/) to see how to install Mongo DB.

## Getting It Running

Clone the project and copy the app directory into the `src` folder in your `GOPATH` directory.

Then run: 
```
dep ensure
```
from within the app directory.

Export port on which you would like to run the server: 
```bash
export PORT=<YOUR_PORT>
```
---
Also export `GOPATH` if you are running from outside your Gopath or Goroot. If your app folder path is `/home/user/workspace/src/app`, the path you export should be `/home/user/workspace/`
___

You will also need a .env file to get the server running. The environment keys are as follows: 
```
SERVER         # MongoDB server
DBNAME         # Name of DB in use
TOKENSECRET    
TOKENTIMEOUT   # in minutes

# Amazon web service creds. Not mandatory.
AWS_REGION
AWS_KEY
AWS_SECRET
AWS_BUCKET
```
___

Run command:
```bash
go run main.go
```
to get the server up and running.

## Build
Go to your `/app` folder and run:
```bash
go build
```
You should see an executable in your folder.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## Have Fun Coding... :)